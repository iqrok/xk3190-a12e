const XK3190 = require('..');
const { A12E, Finder } = XK3190;

const a12e = new A12E();

(async () => {
	const config = await Finder.find(1);

	if(!config){
		console.error('No device found');
		return 1;
	}

	a12e.init(config);
	a12e.on('data', data => console.log(data));

})();
