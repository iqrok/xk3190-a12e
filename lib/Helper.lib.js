/**
 * @class
 * @classdesc XK3190-A12E helper class.
 * @extends events
 * */
class XK3190_A12E_HELPER {
	/**
	 * sleep asynchrounously for given period
	 * @private
	 * @param {number} ms - milliseconds to sleep
	 * */
	sleep(ms){
		return new Promise(resolve => setTimeout(resolve, ms));
	}

	/**
	 * Round number to fixed decimal digit
	 * @private
	 * @param {number} num - number to be rounded
	 * @param {number} digit - number of decimal
	 * @returns {number} rounded number
	 * */
	round(num, digit) {
		const pow = Math.pow(10, digit);
		return Math.round((num + Number.EPSILON) * pow) / pow;
	}

	/**
	 * Extract info from buffer
	 * @private
	 * @param {Buffer} buffer - streamed data received from the XK3190_A12E device
	 * @returns {null|object} regex match or null if no match found
	 * */
	match(buffer){
		try {
			return buffer.toString().match(/w([a-z])([0-9\.\-]+)([a-zA-Z]+)/);
		} catch (error) {
			console.error('_match', error);
			return null;
		}
	}
}

module.exports = new XK3190_A12E_HELPER();
