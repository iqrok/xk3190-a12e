const EventEmitter = require('events');
const SerialHelper = require('@iqrok/serial.helper');

const { round, match } = require('./Helper.lib.js');
const Finder = require('./Finder.lib.js');

/**
 * @class
 * @classdesc class represents XK3190_A12E device.
 * @extends events
 * */
class XK3190_A12E extends EventEmitter {

	static STATE = {
		RUNNING: 0,
		SEARCHING: 1,
		STOPPED: 400,
		TIMEOUT: 408,
	};

	static WEIGHING_TYPE = {
		n: 'nett',
		g: 'gross',
	};

	constructor(config){
		super();
		const self = this;

		if(config) self.init(config);
	}

	/**
	 * Intitalize XK3190_A12E communication
	 * @param {Object} config - XK3190_A12E configuration
	 * @param {string} config.port - XK3190_A12E port path
	 * @param {number} config.baud - XK3190_A12E baud rate
	 * @param {number} [config.timeout=1000] - how long, in milliseconds, the XK3190_A12E device is considered powered off if no data is sent from it
	 * @param {number} [config.digit=2] - number of decimal in reported value
	 * @param {Object} config.stability - parameters on how streamed value is considered stable
	 * @param {number} config.stability.limit - value below this won't be considered as valid value
	 * @param {number} config.stability.diff - how big the difference from previous stable value would be considere as new stable value
	 * @param {number} config.stability.counter - streamed value must be in the same value for this many times
	 * */
	init(config){
		const self = this;

		const { port, baud, timeout, stability, digit } = config;

		self._config = {
				port,
				baud,
				parser: {
					type: 'InterByteTimeout',
					interval: 30,
				},
			};

		self._digit = digit || 2;
		self._timeout = timeout || 1000;
		self._stability = stability || {
				limit: 0.1,
				diff: 0.1,
				counter: 5,
			};
		self._counter = {
				stable: 0,
			};
		self._last = {
				value: 0,
				stable: 0,
				timestamp: undefined,
			};
		self._flag = {
				stable: false,
				repeat: true,
			};

		self.isOpen = false;
		self.state = XK3190_A12E.STATE.STOPPED;

		self._serial = new SerialHelper(self._config);
		self._serial.on('open', () => { self.isOpen = true; });
		self._serial.on('close', () => { self.isOpen = false; });
		self._serial.on('data', buffer => { self._parse(buffer); });
		self._serial.on('error', buffer => { self.emit('error', buffer); });

		self._startTimeout();

		return self;
	}

	/**
	 * Search XK3190-A12E device from connected serial port
	 * @param {boolean} reinit - re-init connection if device is found
	 * @returns {null|object} device configuration or null if not found
	 * */
	static async find(reinit = false){
		const self = this;

		self.state = XK3190_A12E.STATE.SEARCHING;

		// stop all repeating timeout
		self._flag.repeat = false;
		await self._clearAllTimeouts();

		// stop & delete serial instance
		if(self._serial){
			if(self._serial.port.isOpen){
				self._serial.port.close();
				self._serial.conf.autoreconnect = false;
			}

			delete self._serial;
		}

		const device = await Finder.find();

		if(reinit && device?.port && device?.baud){
			self.init({
					...self._config,
					port: device.port,
					baud: device.baud,
				});

			// re-enable repeating timeout on re-init
			self._flag.repeat = true;
		}

		return device;
	}

	/**
	 * Cleat all timeouts
	 * @private
	 * */
	_clearAllTimeouts(){
		const self = this;

		return new Promise(resolve => {
			const max = setTimeout(() => {
					let latest = max;
					for(let idx = 1; idx <= latest; idx++){
						if(self._iRepeat > latest) latest = self._iRepeat;
						clearTimeout(idx);
					}

					resolve(latest);
				}, 1);
			});
	}

	/**
	 * Start A timeout instance
	 * @private
	 * */
	_startTimeout(){
		const self = this;

		self._iTimeout = setTimeout(() => {
				self.state = XK3190_A12E.STATE.TIMEOUT;
				self._raiseError(
						XK3190_A12E.STATE.TIMEOUT,
						'Timeout',
						{
							cb: () => self.state === XK3190_A12E.STATE.TIMEOUT,
							ms: self._timeout << 1,
						},
					);
			}, self._timeout);
	}

	/**
	 * Parse received stream and emit parsed data
	 * @private
	 * @param {Buffer} buffer - streamed data received from the XK3190_A12E device
	 * */
	_parse(buffer) {
		const self = this;

		const timestamp = Date.now();
		const isTimeout = self._last.timestamp
			&& (timestamp - self._last.timestamp) >= self._timeout;

		self._last.timestamp = timestamp;

		if(isTimeout) return;

		// regex input
		const matches = match(buffer);

		if(!matches) return;

		// clear timeout as data is received
		clearTimeout(self._iTimeout);

		self.state = XK3190_A12E.STATE.RUNNING;

		// parse data according to datasheet.
		// Omit checksum as it's not always available
		const type = XK3190_A12E.WEIGHING_TYPE[matches[1]];
		const value = round(Number(matches[2]), self._digit);
		const unit = matches[3];

		// only allow small step difference to indicate stability
		const isInRange = Math.abs(self._last.value - value) <= self._stability.diff;
		const isAboveLimit = value >= self._stability.limit;

		// savel last value to indicate stability
		self._last.value = value;

		if(isInRange && isAboveLimit){
			const isCounterAbove = ++self._counter.stable > self._stability.counter;
			if(isCounterAbove && !self._flag.stable) self._flag.stable = true;
		} else {
			self._flag.stable = false;
			self._counter.stable = 0;

			// reset last stable if nothing is placed on the weigh scale
			if(value <= self._stability.limit) self._last.stable = 0;
		}

		self.emit('data', {
				timestamp,
				type,
				stable: self._flag.stable,
				empty: !isAboveLimit,
				value,
				unit,
			});

		self._startTimeout();
	}

	/**
	 * Raise error and emit the error
	 * @private
	 * @param {number} code - error code
	 * @param {string} message - error message
	 * @param {Object} [repeat=null] - if not null, then error will be raised according to the this condition
	 * @param {Function} repeat.cb - callback to check condition to repeat raising error
	 * @param {number} repeat.ms - interval in milliseconds for the next time error will be raised
	 * */
	_raiseError(code, message, repeat = null){
		const self = this;

		const timestamp = Date.now();
		self.emit('data', {
				timestamp,
				error: { code, message },
			});

		if(repeat.cb instanceof Function && repeat.cb()){
			self._iRepeat = setTimeout(
					() => self._flag.repeat
						&& repeat.cb()
						&& self._raiseError(code, message, repeat),
					repeat.ms,
				);
		}
	}

	get weight() {
		return this._last.value;
	}
};

module.exports = XK3190_A12E;
