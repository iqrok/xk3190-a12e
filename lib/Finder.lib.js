const fs = require('fs');
const { platform } = require('process');
const SerialHelper = require('@iqrok/serial.helper');

const { sleep, match } = require('./Helper.lib.js');

/**
 * @class
 * @classdesc class to search XK3190-A12E device.
 * @extends events
 * */
class XK3190_A12E_FINDER {

	/**
	 * List available serial port(s)
	 * @returns {string[]} list of available serial port path
	 * */
	async list(){
		switch(platform){
			case 'linux': {
				const sDirPath = '/dev/serial/by-id';

				if(!fs.existsSync(sDirPath)) {
					throw new Error('No Serial device is attached!');
				}

				const list = await fs.promises.readdir(sDirPath);
				return list.map(item => `${sDirPath}/${item}`);
			} break;

			default: {
				const coms = await SerialHelper.list();
				const list = [];

				for(const item of coms){
					const valid = item.manufacturer
						|| item.serialNumber
						|| item.pnpId
						|| item.locationId
						|| item.vendorId
						|| item.productId;

					if(valid) list.push(item.path);
				}

				return list;
			} break;
		}
	}

	/**
	 * Find XK3190-A12E by comparing received bytes to its valid format
	 * @returns {null|object} device configuration or null if not found
	 * */
	async find(verbose = 0){
		const self = this;

		const sBps = [ 9600, 4800, 2400, 1200 ];

		let sConfig;
		let isPortFound = false;

		const timer = {
				_timeout: 1_000, // 5s timeout
				_start: 0,
				start: function() { this._start = Date.now(); },
				isTimedOut: function(){
						return Date.now() - this._start > this._timeout
					},
			};

		for(const port of await self.list()){
			for(const baud of sBps){
				sConfig = { port, baud };

				if(verbose > 0){
					console.log(`Attempting '${port}' at ${baud} bps...`);
				}

				let serial = new SerialHelper({
						...sConfig,
						autoreconnect: false,
						parser: {
							type: 'InterByteTimeout',
							interval: 30,
						},
					});

				let received = undefined;

				const cb = {
						data: buffer => { received = buffer; },
						error: error => { console.error(error) },
					};

				for(const ev in cb) serial.on(ev, cb[ev]);

				timer.start();
				while(!received) {
					await sleep(50);
					if(timer.isTimedOut()) break;
				}

				for(const ev in cb) serial.removeAllListeners(ev);

				serial.disconnect();

				delete serial.port;

				if(!received) continue;

				isPortFound = match(received);
				if(isPortFound) break;
			}

			if(isPortFound) break;
		}

		return isPortFound
			? { status: Boolean(isPortFound), ...sConfig }
			: null;
	}
}

module.exports = new XK3190_A12E_FINDER();
